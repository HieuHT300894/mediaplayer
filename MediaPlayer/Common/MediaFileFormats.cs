﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MediaPlayer.Common
{
    public class MediaFileFormats
    {
        static readonly object _lockedFileFormats = new object();
        static readonly object _lockedFileExtensions = new object();
        static Dictionary<string, string[]> _fileFormats;
        static string[] _fileExtensions;

        public static Dictionary<string, string[]> GetFileFormats()
        {
            if (_fileFormats != null)
                return _fileFormats;

            lock (_lockedFileFormats)
            {
                if (_fileFormats != null)
                    return _fileFormats;

                _fileFormats = new Dictionary<string, string[]>(StringComparer.OrdinalIgnoreCase);

                _fileFormats["AIF audio file"] = new string[] { ".aif" };
                _fileFormats["CD audio track file"] = new string[] { ".cda" };
                _fileFormats["MIDI audio file"] = new string[] { ".mid", ".midi" };
                _fileFormats["MP3 audio file"] = new string[] { ".mp3" };
                _fileFormats["MPEG-2 audio file"] = new string[] { ".mpa" };
                _fileFormats["Ogg Vorbis audio file"] = new string[] { ".ogg" };
                _fileFormats["WAV file"] = new string[] { ".wav" };
                _fileFormats["WMA audio file"] = new string[] { ".wma" };
                _fileFormats["Windows Media Player playlist"] = new string[] { ".wpl" };

                _fileFormats["3GPP2 multimedia file"] = new string[] { ".3g2" };
                _fileFormats["3GPP multimedia file"] = new string[] { ".3gp" };
                _fileFormats["AVI file"] = new string[] { ".avi" };
                _fileFormats["Adobe Flash file"] = new string[] { ".flv" };
                _fileFormats["H.264 video file"] = new string[] { ".h264" };
                _fileFormats["Apple MP4 video file"] = new string[] { ".m4v" };
                _fileFormats["Matroska Multimedia Container"] = new string[] { ".mkv" };
                _fileFormats["Apple QuickTime movie file"] = new string[] { ".mov" };
                _fileFormats["MPEG4 video file"] = new string[] { ".mp4" };
                _fileFormats["MPEG video file"] = new string[] { ".mpg", ".mpeg" };
                _fileFormats["RealMedia file"] = new string[] { ".rm" };
                _fileFormats["Shockwave flash file"] = new string[] { ".swf" };
                _fileFormats["DVD Video Object"] = new string[] { ".vob" };
                _fileFormats["Windows Media Video file"] = new string[] { ".wmv" };

                return _fileFormats;
            }
        }

        public static string[] GetFileExtensions()
        {
            if (_fileExtensions != null)
                return _fileExtensions;

            lock (_lockedFileExtensions)
            {
                if (_fileExtensions != null)
                    return _fileExtensions;

                _fileExtensions = GetFileFormats().SelectMany(x => x.Value).ToArray();

                return _fileExtensions;
            }
        }

        static void LoadFileFormatsConfig()
        {
            try
            {
                var appReg = Registry.CurrentUser.OpenSubKey(Path.Combine("SOFTWARE", "MediaPlayer"));
                if (appReg != null)
                {
                    var fileFormats = appReg.OpenSubKey("FileFormats");
                    if (fileFormats != null)
                    {
                        var names = fileFormats.GetValueNames();
                        foreach (var name in names)
                        {
                            var values = fileFormats.GetValue(name, "").ToString().Split("".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
    }
}
