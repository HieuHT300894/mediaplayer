﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayer.Models
{
    public class VolumeBarModel: BaseModel
    {
        double _value = 0.5;

        public double MinValue
        {
            get
            {
                return 0;
            }
        }

        public double MaxValue
        {
            get
            {
                return 1;
            }
        }

        public double Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (value < MinValue)
                    _value = MinValue;
                else if (value > MaxValue)
                    _value = MaxValue;
                else
                    _value = value;

                OnPropertyChanged("Value");
                OnPropertyChanged("Percent");
            }
        }

        public string Percent
        {
            get
            {
                return Math.Round((Value / (MaxValue - MinValue)) * 100, 0).ToString();
            }
        }
    }
}
