﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MediaPlayer.Models
{
    public class MediaElementExtModel : BaseModel
    {
        TimeSpan _mediaCurrentTime;
        TimeSpan _mediaTotalTime;

        public Uri MediaFile { get; set; }

        public TimeSpan MediaCurrentTime
        {
            get
            {
                return _mediaCurrentTime;
            }
            set
            {
                _mediaCurrentTime = value;

                OnPropertyChanged("MediaCurrentTime");
                OnPropertyChanged("MediaCurrentSeconds");
            }
        }

        public TimeSpan MediaTotalTime
        {
            get
            {
                return _mediaTotalTime;
            }
            set
            {
                _mediaTotalTime = value;

                OnPropertyChanged("MediaTotalTime");
                OnPropertyChanged("MediaTotalSeconds");
            }
        }

        public double MediaCurrentSeconds
        {
            get
            {
                return MediaCurrentTime.TotalSeconds;
            }
        }

        public double MediaTotalSeconds
        {
            get
            {
                return MediaTotalTime.TotalSeconds > 0 ? MediaTotalTime.TotalSeconds : 1;
            }
        }

        public string MediaFileName
        {
            get
            {
                if (MediaFile != null)
                    return MediaFile.LocalPath;
                return null;
            }
        }
    }
}
