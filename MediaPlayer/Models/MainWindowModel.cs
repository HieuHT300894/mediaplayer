﻿using System.Windows;

namespace MediaPlayer.Models
{
    public class MainWindowModel : BaseModel
    {
        public bool IsFullScreen { get; set; }

        public bool IsMaximized { get; set; }

        public double MinWidth
        {
            get
            {
                return 362;
            }
        }

        public double MinHeight
        {
            get
            {
                return 108;
            }
        }
    }
}
