﻿namespace MediaPlayer.Models
{
    public class MediaControlBarModel : BaseModel
    {
        bool _isVisible = true;
        System.Windows.Controls.MediaState _mediaState;

        public bool? IsSliderSelection { get; set; }

        public bool IsVisible
        {
            get
            {
                return _isVisible;
            }
            set
            {
                _isVisible = value;

                OnPropertyChanged("IsVisible");
                OnPropertyChanged("AllowDrop");
            }
        }

        public System.Windows.Controls.MediaState MediaState
        {
            get
            {
                return _mediaState;
            }
            set
            {
                _mediaState = value;

                OnPropertyChanged("AllowSliderDrag");
                OnPropertyChanged("IsEnablePlayMediaButton");
                OnPropertyChanged("IsEnablePauseMediaButton");
                OnPropertyChanged("IsEnableStopMediaButton");
                OnPropertyChanged("IsEnableRewindMediaButton");
                OnPropertyChanged("IsEnableFastForwardMediaButton");
                OnPropertyChanged("IsEnableNextMediaButton");
                OnPropertyChanged("IsEnableForwardMediaButton");
            }
        }

        public bool AllowDrop
        {
            get
            {
                return true;
            }
        }

        public bool AllowSliderDrag
        {
            get
            {
                switch (_mediaState)
                {
                    case System.Windows.Controls.MediaState.Play:
                    case System.Windows.Controls.MediaState.Pause:
                    case System.Windows.Controls.MediaState.Stop:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsEnablePlayMediaButton
        {
            get
            {
                switch (_mediaState)
                {
                    case System.Windows.Controls.MediaState.Play:
                        return false;
                    case System.Windows.Controls.MediaState.Pause:
                    case System.Windows.Controls.MediaState.Stop:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsEnablePauseMediaButton
        {
            get
            {
                switch (_mediaState)
                {
                    case System.Windows.Controls.MediaState.Play:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsEnableStopMediaButton
        {
            get
            {
                switch (_mediaState)
                {
                    case System.Windows.Controls.MediaState.Play:
                    case System.Windows.Controls.MediaState.Pause:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsEnableRewindMediaButton
        {
            get
            {
                switch (_mediaState)
                {
                    case System.Windows.Controls.MediaState.Play:
                    case System.Windows.Controls.MediaState.Pause:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsEnableFastForwardMediaButton
        {
            get
            {
                switch (_mediaState)
                {
                    case System.Windows.Controls.MediaState.Play:
                    case System.Windows.Controls.MediaState.Pause:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsEnableNextMediaButton
        {
            get
            {
                switch (_mediaState)
                {
                    case System.Windows.Controls.MediaState.Play:
                    case System.Windows.Controls.MediaState.Pause:
                    case System.Windows.Controls.MediaState.Stop:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsEnableForwardMediaButton
        {
            get
            {
                switch (_mediaState)
                {
                    case System.Windows.Controls.MediaState.Play:
                    case System.Windows.Controls.MediaState.Pause:
                    case System.Windows.Controls.MediaState.Stop:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}
