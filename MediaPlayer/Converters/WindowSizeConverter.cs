﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

namespace MediaPlayer.Converters
{
    internal static class WindowSizeConverter
    {
        internal static Point GetFullScreenPoint()
        {
            var left = (SystemParameters.MaximumWindowTrackWidth - SystemParameters.PrimaryScreenWidth) / 2;
            var top = (SystemParameters.MaximumWindowTrackHeight - SystemParameters.PrimaryScreenHeight) / 2;

            return new Point(left * -1, top * -1);
        }

        internal static Size GetFullScreenSize(Window window)
        {
            var matrix = GetDPIFactors(window);
            var dpiWidthFactor = matrix.M11;
            var dpiHeightFactor = matrix.M22;

            var width = SystemParameters.MaximumWindowTrackWidth - SystemParameters.FixedFrameVerticalBorderWidth;
            var height = SystemParameters.MaximumWindowTrackHeight - SystemParameters.FixedFrameHorizontalBorderHeight;

            return new Size(width * dpiWidthFactor, height * dpiHeightFactor);
        }

        internal static Point GetMaximizedPoint()
        {
            return new Point(0, 0);
        }

        internal static Size GetMaximizedSize(Window window)
        {
            var matrix = GetDPIFactors(window);
            var dpiWidthFactor = matrix.M11;
            var dpiHeightFactor = matrix.M22;

            var width = SystemParameters.MaximizedPrimaryScreenWidth + SystemParameters.Border;
            var height = SystemParameters.MaximizedPrimaryScreenHeight + SystemParameters.Border;

            return new Size(width * dpiWidthFactor, height * dpiHeightFactor);
        }

        static Matrix GetDPIFactors(Window window)
        {
            var windowSource = PresentationSource.FromVisual(window);
            return PresentationSource.FromVisual(window).CompositionTarget.TransformToDevice;
        }
    }
}
