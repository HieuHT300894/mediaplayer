﻿using MediaPlayer.Common;
using MediaPlayer.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.Windows.Interop;

namespace MediaPlayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainWindowModel _mainWindowModel;
        VolumeBarModel _volumeBarModel;
        MediaControlBarModel _mediaControlBarModel;
        MediaElementExtModel _mediaElementExtModel;
        double _seekSeconds = 10;
        double _totalSeekSeconds;
        double _seekVolume = 0.01;
        double _totalSeekVolume;
        DispatcherTimer _mediaTimer;
        DispatcherTimer _mouseClickTimer;
        MediaElement _mediaControl;
        Dictionary<int, double> _mediaRecentFiles;
        List<string> _mediaPlaylistFiles;
        List<string> _uploadedFiles;
        WindowState _lastWindowState;
        WindowStyle _lastWindowStyle;
        bool _hasFitContentSize;

        public MainWindow()
        {
            _mainWindowModel = App.MainWindowModel;
            _volumeBarModel = App.VolumeBarModel;
            _mediaControlBarModel = App.MediaControlBarModel;
            _mediaElementExtModel = App.MediaElementExtModel;

            _mediaTimer = new DispatcherTimer();
            _mediaTimer.Interval = TimeSpan.FromSeconds(1);
            _mediaTimer.Tick += MediaTimer_Tick;

            _mouseClickTimer = new DispatcherTimer();
            _mouseClickTimer.Interval = TimeSpan.FromMilliseconds(200);
            _mouseClickTimer.Tick += MouseClickTimer_Tick;

            _mediaRecentFiles = new Dictionary<int, double>();
            _mediaPlaylistFiles = new List<string>();
            _uploadedFiles = new List<string>();

            InitializeComponent();
            DataContext = _mainWindowModel;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
            {
                LoadConfig();
                InitializeMediaPlayer();
            }));
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            SaveConfig();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F11)
            {
                DisplayFullScreen();
            }
            else if (e.Key == Key.Escape)
            {
                if (_mainWindowModel.IsFullScreen)
                    DisplayFullScreen();
            }
            else if (e.Key == Key.Space)
            {
                if (_mediaControlBarModel.MediaState != MediaState.Manual)
                {
                    if (_mediaControlBarModel.MediaState == MediaState.Play)
                        PauseMedia();
                    else
                        PlayMedia();
                }
            }
            else if (e.Key == Key.Left || e.Key == Key.Right)
            {
                _totalSeekSeconds += _seekSeconds;
            }
            else if (e.Key == Key.Up || e.Key == Key.Down)
            {
                _totalSeekVolume += _seekVolume;
            }
            else if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                if (e.Key == Key.O)
                {
                    var fileName = OpenFile();
                    if (!string.IsNullOrWhiteSpace(fileName))
                        LoadMediaFile(ParseFileNameToUri(fileName));
                }
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left || e.Key == Key.Right)
            {
                if (e.Key == Key.Left)
                    SeekMediaPosition(true);
                else
                    SeekMediaPosition(false);
            }
            else if (e.Key == Key.Up || e.Key == Key.Down)
            {
                if (e.Key == Key.Up)
                    SeekMediaVolume(false);
                else
                    SeekMediaVolume(true);
            }
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                _mainWindowModel.IsMaximized = false;
            }
            else if (WindowState == WindowState.Maximized)
            {
                _mainWindowModel.IsMaximized = true;
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Height <= _mainWindowModel.MinHeight)
                _mediaControlBarModel.IsVisible = true;
        }

        private void Media_Loaded(object sender, RoutedEventArgs e)
        {
            _mediaControl = sender as MediaElement;
        }

        private void Media_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (_mediaControl.NaturalDuration.HasTimeSpan)
            {
                _mediaControl.Position = TimeSpan.FromSeconds(_mediaRecentFiles[_mediaElementExtModel.MediaFile.LocalPath.GetHashCode()]);
                _mediaElementExtModel.MediaTotalTime = _mediaControl.NaturalDuration.TimeSpan;

                DisplayFitContentScren();
            }
        }

        private void Media_MediaEnded(object sender, RoutedEventArgs e)
        {
            StopMedia();
        }

        private void Media_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            MessageBox.Show(e.ErrorException.ToString());
        }

        private void MediaTimer_Tick(object sender, EventArgs e)
        {
            if (_mediaControl.NaturalDuration.HasTimeSpan)
            {
                if (_mediaControlBarModel.IsSliderSelection == null)
                {
                    _mediaElementExtModel.MediaCurrentTime = _mediaControl.Position;
                }
            }
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (_mediaControl.NaturalDuration.HasTimeSpan)
            {
                if (_mediaControlBarModel.IsSliderSelection == true)
                {
                    _mediaElementExtModel.MediaCurrentTime = TimeSpan.FromSeconds(e.NewValue);
                }
                else if (_mediaControlBarModel.IsSliderSelection == false)
                {
                    _mediaControl.Position = TimeSpan.FromSeconds(e.NewValue);
                }
            }
            else if (_mediaControlBarModel.IsSliderSelection == false)
            {
                _mediaElementExtModel.MediaCurrentTime = TimeSpan.FromSeconds(e.NewValue);
            }
        }

        private void PlayMediaButton_Click(object sender, RoutedEventArgs e)
        {
            PlayMedia();
        }

        private void PauseMediaButton_Click(object sender, RoutedEventArgs e)
        {
            PauseMedia();
        }

        private void StopMediaButton_Click(object sender, RoutedEventArgs e)
        {
            StopMedia();
        }

        private void RewindMediaButton_Click(object sender, RoutedEventArgs e)
        {
            SeekMediaPosition(true, _seekSeconds);
        }

        private void FastForwardMediaButton_Click(object sender, RoutedEventArgs e)
        {
            SeekMediaPosition(false, _seekSeconds);
        }

        private void ForwardMediaButton_Click(object sender, RoutedEventArgs e)
        {
            ForwardMedia();
        }

        private void NextMediaButton_Click(object sender, RoutedEventArgs e)
        {
            NextMedia();
        }

        private void MouseClickTimer_Tick(object sender, EventArgs e)
        {
            _mouseClickTimer.IsEnabled = false;

            var mouse = _mouseClickTimer.Tag as MouseButtonEventArgs;
            if (mouse != null)
            {
                if (mouse.ChangedButton == MouseButton.Left)
                {
                    if (mouse.ClickCount == 1)
                    {
                        if (_mediaControlBarModel.MediaState != MediaState.Manual)
                        {
                            if (_mediaControlBarModel.MediaState == MediaState.Play)
                                PauseMedia();
                            else
                                PlayMedia();
                        }
                    }
                    else if (mouse.ClickCount == 2)
                    {
                        DisplayFullScreen();
                    }
                }
            }
        }

        private void MediaBorder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _mouseClickTimer.Tag = e;

            if (!_mouseClickTimer.IsEnabled)
                _mouseClickTimer.IsEnabled = true;
        }

        private void MediaBorder_MouseEnter(object sender, MouseEventArgs e)
        {
            _mediaControlBarModel.IsVisible = false;
        }

        private void MediaFiles_DragEnter(object sender, DragEventArgs e)
        {
            _uploadedFiles.Clear();

            var data = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (data != null)
                _uploadedFiles.AddRange(data);
        }

        private void MediaFiles_DragOver(object sender, DragEventArgs e)
        {
            if (_uploadedFiles != null && _uploadedFiles.Count > 0)
                e.Effects = DragDropEffects.Move;
            else
                e.Effects = DragDropEffects.None;
            e.Handled = true;
        }

        private void MediaFiles_Drop(object sender, DragEventArgs e)
        {
            _mediaPlaylistFiles.Clear();
            foreach (var fileName in _uploadedFiles)
            {
                var tmpFiles = new List<string>();

                if (File.GetAttributes(fileName) == FileAttributes.Directory)
                    tmpFiles.AddRange(Directory.GetFiles(fileName, "*", SearchOption.AllDirectories));
                else
                    tmpFiles.Add(fileName);

                foreach (var tmpFile in tmpFiles)
                {
                    if (CheckFileIsAudioOrVideo(tmpFile))
                        _mediaPlaylistFiles.Add(tmpFile);
                }
            }

            if (_mediaPlaylistFiles.Count > 0)
                LoadMediaFile(ParseFileNameToUri(_mediaPlaylistFiles.First()));
        }

        private void MediaControlBar_MouseEnter(object sender, MouseEventArgs e)
        {
            _mediaControlBarModel.IsVisible = true;
        }

        void DisplayFullScreen()
        {
            _mainWindowModel.IsFullScreen = !_mainWindowModel.IsFullScreen;

            if (_mainWindowModel.IsFullScreen)
            {
                _lastWindowStyle = WindowStyle;
                _lastWindowState = WindowState;

                WindowStyle = WindowStyle.None;

                if (WindowState == WindowState.Maximized)
                    WindowState = WindowState.Normal;
                WindowState = WindowState.Maximized;
            }
            else
            {
                WindowStyle = _lastWindowStyle;
                WindowState = _lastWindowState;
            }
        }

        void DisplayMaximizedScreen()
        {
            if (WindowState == WindowState.Normal)
                WindowState = WindowState.Maximized;
            else if (WindowState == WindowState.Maximized)
                WindowState = WindowState.Normal;
        }

        void DisplayFitContentScren()
        {
            if (!_hasFitContentSize)
            {
                _hasFitContentSize = true;

                if (_mediaControl.HasVideo)
                {
                    // ...
                }
                else
                {
                    Width = _mainWindowModel.MinWidth;
                    Height = _mainWindowModel.MinHeight;
                }
            }
        }

        void SeekMediaPosition(bool isRewind, double? seekingValue = null)
        {
            if (_mediaControl.NaturalDuration.HasTimeSpan)
            {
                if (seekingValue.HasValue)
                    _totalSeekSeconds = seekingValue.Value;

                if (isRewind)
                    _totalSeekSeconds *= -1;

                var newValue = TimeSpan.FromSeconds(_mediaControl.Position.TotalSeconds + _totalSeekSeconds);
                _mediaControl.Position = newValue;
                _mediaElementExtModel.MediaCurrentTime = newValue;

                _totalSeekSeconds = 0;
            }
        }

        void SeekMediaVolume(bool isVolumeDown, double? seekingValue = null)
        {
            if (seekingValue.HasValue)
                _totalSeekVolume = seekingValue.Value;

            if (isVolumeDown)
                _totalSeekVolume *= -1;

            _volumeBarModel.Value = Math.Round(((_volumeBarModel.Value * 100) + (_totalSeekVolume * 100)) / 100, 2);

            _totalSeekVolume = 0;
        }

        void LoadConfig()
        {
            try
            {
                var appReg = Registry.CurrentUser.OpenSubKey(Path.Combine("SOFTWARE", "MediaPlayer"));
                if (appReg != null)
                {
                    _volumeBarModel.Value = Convert.ToDouble(appReg.GetValue("MediaVolume", 0.5));

                    var recentFilesReg = appReg.OpenSubKey("MediaFiles");
                    if (recentFilesReg != null)
                    {
                        var names = recentFilesReg.GetValueNames();
                        foreach (var name in names)
                        {
                            var key = 0;
                            if (int.TryParse(name, out key))
                            {
                                _mediaRecentFiles.Add(key, Convert.ToDouble(recentFilesReg.GetValue(name, 0)));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        void SaveConfig()
        {
            try
            {
                SaveMediaPosition();

                var appReg = Registry.CurrentUser.CreateSubKey(Path.Combine("SOFTWARE", "MediaPlayer"), RegistryKeyPermissionCheck.ReadWriteSubTree);
                appReg.SetValue("MediaVolume", _volumeBarModel.Value);

                var recentFilesReg = appReg.CreateSubKey("MediaFiles", RegistryKeyPermissionCheck.ReadWriteSubTree);
                foreach (var item in _mediaRecentFiles)
                {
                    if (item.Value > 0)
                        recentFilesReg.SetValue(item.Key.ToString(), item.Value);
                    else
                        recentFilesReg.DeleteValue(item.Key.ToString(), false);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        void InitializeMediaPlayer()
        {
            var args = Environment.GetCommandLineArgs();
            if (args != null && args.Length > 1)
            {
                LoadMediaFile(ParseFileNameToUri(args[1]));
            }
        }

        string OpenFile()
        {
            var result = "";
            try
            {
                var dialog = new OpenFileDialog();
                if (dialog.ShowDialog() == true)
                {
                    result = dialog.FileName;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return result;
        }

        void LoadMediaFile(Uri fileUri)
        {
            if (fileUri != null)
            {
                var isSameFile = false;

                if (_mediaElementExtModel.MediaFile != null)
                {
                    isSameFile = _mediaElementExtModel.MediaFile.LocalPath.GetHashCode() == fileUri.LocalPath.GetHashCode();
                }

                if (!isSameFile)
                {
                    SaveMediaPosition(fileUri);
                    StopMedia();

                    _mediaControlBarModel.MediaState = MediaState.Manual;
                    _mediaElementExtModel.MediaFile = fileUri;
                    _mediaElementExtModel.OnPropertyChanged("MediaFile");
                    _mediaElementExtModel.OnPropertyChanged("MediaFileName");

                    PlayMedia();
                }
            }
        }

        void PlayMedia()
        {
            _mediaTimer.Start();

            _mediaControlBarModel.MediaState = MediaState.Play;
            _mediaControlBarModel.OnPropertyChanged("MediaState");
        }

        void PauseMedia()
        {
            _mediaTimer.Stop();

            _mediaControlBarModel.MediaState = MediaState.Pause;
            _mediaControlBarModel.OnPropertyChanged("MediaState");
        }

        void StopMedia()
        {
            _mediaTimer.Stop();

            _mediaControlBarModel.MediaState = MediaState.Stop;
            _mediaControlBarModel.OnPropertyChanged("MediaState");

            _mediaElementExtModel.MediaCurrentTime = TimeSpan.FromSeconds(0);
        }

        Uri ParseFileNameToUri(string fileName)
        {
            var fileUri = default(Uri);
            if (CheckFileIsAudioOrVideo(fileName))
                Uri.TryCreate(fileName, UriKind.RelativeOrAbsolute, out fileUri);
            return fileUri;
        }

        void NextMedia()
        {
            if (_mediaPlaylistFiles != null && _mediaElementExtModel.MediaFile != null)
            {
                if (_mediaPlaylistFiles.Count > 1)
                {
                    var currentFileName = _mediaElementExtModel.MediaFile.LocalPath;
                    var currentFileIndex = _mediaPlaylistFiles.FindIndex(x => x.Equals(currentFileName, StringComparison.OrdinalIgnoreCase));
                    if (currentFileIndex != -1)
                    {
                        var nextFileIndex = currentFileIndex + 1;
                        if (nextFileIndex < _mediaPlaylistFiles.Count)
                        {
                            LoadMediaFile(ParseFileNameToUri(_mediaPlaylistFiles[nextFileIndex]));
                        }
                    }
                }
                else
                {
                    var currentFileName = _mediaElementExtModel.MediaFile.LocalPath;
                    var currentDirectory = Path.GetDirectoryName(currentFileName);
                    var files = Directory.EnumerateFiles(currentDirectory).OrderBy(x => x.Length).ThenBy(x => x).ToList();
                    var currentFileIndex = files.FindIndex(x => x.Equals(currentFileName, StringComparison.OrdinalIgnoreCase));
                    if (currentFileIndex != -1)
                    {
                        var nextFileIndex = currentFileIndex + 1;
                        if (nextFileIndex < files.Count)
                        {
                            LoadMediaFile(ParseFileNameToUri(files[nextFileIndex]));
                        }
                    }
                }
            }
        }

        void ForwardMedia()
        {
            if (_mediaPlaylistFiles != null && _mediaElementExtModel.MediaFile != null)
            {
                if (_mediaPlaylistFiles.Count > 1)
                {
                    var currentFileName = _mediaElementExtModel.MediaFile.LocalPath;
                    var currentFileIndex = _mediaPlaylistFiles.FindIndex(x => x.Equals(currentFileName, StringComparison.OrdinalIgnoreCase));
                    if (currentFileIndex != -1)
                    {
                        var nextFileIndex = currentFileIndex - 1;
                        if (nextFileIndex >= 0)
                        {
                            LoadMediaFile(ParseFileNameToUri(_mediaPlaylistFiles[nextFileIndex]));
                        }
                    }
                }
                else
                {
                    var currentFileName = _mediaElementExtModel.MediaFile.LocalPath;
                    var currentDirectory = Path.GetDirectoryName(currentFileName);
                    var files = Directory.EnumerateFiles(currentDirectory).OrderBy(x => x.Length).ThenBy(x => x).ToList();
                    var currentFileIndex = files.FindIndex(x => x.Equals(currentFileName, StringComparison.OrdinalIgnoreCase));
                    if (currentFileIndex != -1)
                    {
                        var nextFileIndex = currentFileIndex - 1;
                        if (nextFileIndex >= 0)
                        {
                            LoadMediaFile(ParseFileNameToUri(files[nextFileIndex]));
                        }
                    }
                }
            }
        }

        void SaveMediaPosition(Uri fileUri = null)
        {
            // Save current media position
            if (_mediaElementExtModel.MediaFile != null)
            {
                _mediaRecentFiles[_mediaElementExtModel.MediaFile.LocalPath.GetHashCode()] = _mediaControl.Position.TotalSeconds;
            }

            // Save new media position
            if (fileUri != null)
            {
                var hashCode = fileUri.LocalPath.GetHashCode();
                if (!_mediaRecentFiles.ContainsKey(hashCode))
                    _mediaRecentFiles[hashCode] = 0;
            }
        }

        bool CheckFileIsAudioOrVideo(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                return false;

            var fileExtension = Path.GetExtension(fileName);

            if (string.IsNullOrWhiteSpace(fileExtension))
                return false;

            return MediaFileFormats.GetFileExtensions().Contains(fileExtension, StringComparer.OrdinalIgnoreCase);
        }
    }
}
