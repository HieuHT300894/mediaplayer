﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace MediaPlayer.Controls
{
    /// <summary>
    /// Interaction logic for SliderExt.xaml
    /// </summary>
    public partial class SliderExt : Slider
    {
        public static readonly DependencyProperty IsSelectionProperty = DependencyProperty.Register("IsSelection", typeof(bool?), typeof(SliderExt));

        public static readonly DependencyProperty AllowDragProperty = DependencyProperty.Register("AllowDrag", typeof(bool), typeof(SliderExt));

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(SliderExt));

        public bool? IsSelection
        {
            get { return (bool?)GetValue(IsSelectionProperty); }
            set { SetValue(IsSelectionProperty, value); }
        }

        public bool AllowDrag
        {
            get { return Convert.ToBoolean(GetValue(AllowDragProperty)); }
            set { SetValue(AllowDragProperty, value); }
        }

        public string Text
        {
            get { return Convert.ToString(GetValue(TextProperty)); }
            set { SetValue(TextProperty, value); }
        }

        public SliderExt()
        {
            Loaded += SliderExt_Loaded;
        }

        private void SliderExt_Loaded(object sender, RoutedEventArgs e)
        {
            var track = Template.FindName("PART_Track", this) as Track;
            (track.IncreaseRepeatButton.Template.FindName("bdInc", track.IncreaseRepeatButton) as Border).MouseLeftButtonDown += Border_MouseLeftButtonDown;
            (track.DecreaseRepeatButton.Template.FindName("bdDec", track.DecreaseRepeatButton) as Border).MouseLeftButtonDown += Border_MouseLeftButtonDown;
            track.Thumb.DragStarted += Thumb_DragStarted;
            track.Thumb.DragCompleted += Thumb_DragCompleted;
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (AllowDrag)
                UpdateSliderValue((e.GetPosition(this).X / ActualWidth) * Maximum);
            else
                e.Handled = true;
        }

        private void Thumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            if (AllowDrag)
                IsSelection = true;
            else
                (sender as Thumb).CancelDrag();
        }

        private void Thumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            UpdateSliderValue(Value);
        }

        void UpdateSliderValue(double value)
        {
            IsSelection = false;
            OnValueChanged(Value, value);
            IsSelection = null;
        }
    }
}
