﻿using MediaPlayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MediaPlayer.Controls
{
    /// <summary>
    /// Interaction logic for VolumeBar.xaml
    /// </summary>
    public partial class VolumeBar : ProgressBar
    {
        VolumeBarModel _model;
        bool _isMouseCaptured;

        public VolumeBar()
        {
            _model = App.VolumeBarModel;

            InitializeComponent();
            DataContext = _model;
        }

        private void VolumeBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && _isMouseCaptured)
            {
                ConvertPointToVolume(e);
            }
        }

        private void VolumeBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ConvertPointToVolume(e);

            _isMouseCaptured = true;
        }

        private void VolumeBar_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _isMouseCaptured = false;
        }

        void ConvertPointToVolume(MouseEventArgs e)
        {
            var point = e.GetPosition(this);
            var ratio = point.X / ActualWidth;

            _model.Value = ratio * Maximum;
            _model.OnPropertyChanged("Value");
        }
    }
}
