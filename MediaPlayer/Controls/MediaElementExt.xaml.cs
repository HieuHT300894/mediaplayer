﻿using MediaPlayer.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MediaPlayer.Controls
{
    /// <summary>
    /// Interaction logic for MediaElementExt.xaml
    /// </summary>
    public partial class MediaElementExt : MediaElement
    {
        //public static TimeSpan GetBindablePosition(DependencyObject obj)
        //{
        //    return (TimeSpan)obj.GetValue(BindablePositionProperty);
        //}

        //public static void SetBindablePosition(DependencyObject obj, double value)
        //{
        //    obj.SetValue(BindablePositionProperty, value);
        //}

        //// Using a DependencyProperty as the backing store for BindablePosition.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty BindablePositionProperty =
        //    DependencyProperty.RegisterAttached("BindablePosition", typeof(TimeSpan), typeof(MediaElementExt), new PropertyMetadata(TimeSpan.FromSeconds(0), BindablePositionChangedCallback));

        //private static void BindablePositionChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    MediaElement mediaElement = d as MediaElement;
        //    if (mediaElement == null) return;

        //    mediaElement.Position = (TimeSpan)e.NewValue;
        //}

        public MediaElementExt()
        {
            InitializeComponent();
        }
    }

    public class MediaElementHelper
    {
        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.RegisterAttached("Position",
            typeof(TimeSpan), typeof(MediaElementHelper),
            new FrameworkPropertyMetadata(TimeSpan.FromSeconds(0), new PropertyChangedCallback(IsReadOnlyPropertyChanged)));

        private static void IsReadOnlyPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //RichEditControl richEditControl = obj as RichEditControl;

            //if (richEditControl != null)
            //{
            //    richEditControl.ReadOnly = Convert.ToBoolean(e.NewValue);
            //}
        }

        public static void SetPosition(UIElement element, TimeSpan value)
        {
            element.SetValue(PositionProperty, value);
        }

        public static TimeSpan GetPosition(UIElement element)
        {
            return (TimeSpan)element.GetValue(PositionProperty);
        }
    }
}
