﻿using MediaPlayer.Models;
using System.Windows;
using System.Windows.Controls;

namespace MediaPlayer.Controls
{
    /// <summary>
    /// Interaction logic for MediaControlBar.xaml
    /// </summary>
    public partial class MediaControlBar : Border
    {
        public event RoutedEventHandler MediaPlayClick;
        public event RoutedEventHandler MediaPauseClick;
        public event RoutedEventHandler MediaStopClick;
        public event RoutedEventHandler MediaRewindClick;
        public event RoutedEventHandler MediaFastForwardClick;
        public event RoutedEventHandler MediaForwardClick;
        public event RoutedEventHandler MediaNextClick;
        public event RoutedPropertyChangedEventHandler<double> MediaPositionChanged;

        MediaControlBarModel _mediaControlBarModel;
        MediaElementExtModel _mediaElementExtModel;

        public MediaControlBar()
        {
            _mediaControlBarModel = App.MediaControlBarModel;
            _mediaElementExtModel = App.MediaElementExtModel;

            InitializeComponent();
            DataContext = _mediaControlBarModel;
        }

        private void PlayMediaButton_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayClick?.Invoke(sender, e);
        }

        private void PauseMediaButton_Click(object sender, RoutedEventArgs e)
        {
            MediaPauseClick?.Invoke(sender, e);
        }

        private void StopMediaButton_Click(object sender, RoutedEventArgs e)
        {
            MediaStopClick?.Invoke(sender, e);
        }

        private void RewindMediaButton_Click(object sender, RoutedEventArgs e)
        {
            MediaRewindClick?.Invoke(sender, e);
        }

        private void FastForwardMediaButton_Click(object sender, RoutedEventArgs e)
        {
            MediaFastForwardClick?.Invoke(sender, e);
        }

        private void ForwardMediaButton_Click(object sender, RoutedEventArgs e)
        {
            MediaForwardClick?.Invoke(sender, e);
        }

        private void NextMediaButton_Click(object sender, RoutedEventArgs e)
        {
            MediaNextClick?.Invoke(sender, e);
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            MediaPositionChanged?.Invoke(sender, e);
        }
    }
}
