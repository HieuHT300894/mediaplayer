﻿using MediaPlayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace MediaPlayer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static MainWindowModel MainWindowModel;

        public static MediaControlBarModel MediaControlBarModel;

        public static VolumeBarModel VolumeBarModel;

        public static MediaElementExtModel MediaElementExtModel;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MainWindowModel = new MainWindowModel();
            MediaControlBarModel = new MediaControlBarModel();
            VolumeBarModel = new VolumeBarModel();
            MediaElementExtModel = new MediaElementExtModel();
        }
    }
}
